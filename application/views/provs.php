<!DOCTYPE html>
<html>
  <head>
    <title>Sistema Inventarios</title>
 <script>
    var base_url = "<?= base_url() ?>";
  </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?= base_url() ?>static/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color: #EAEDED;">
    <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                <h1><a href="index.html"><img src="<?= base_url()?>static/images/256x256.png" width="5%" height="5%"/> Inventario</a><h1>

                </div>
             </div>
             <div class="col-md-5">
                <div class="row">
                  <div class="col-lg-12">
    </div>
                </div>
             </div>
             <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                          <ul class="dropdown-menu animated fadeInUp">
                            <li><a href="login.html">Salir</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                </div>
             </div>
          </div>
       </div>
  </div>

    <div class="page-content">
      <div class="row">
      <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-user"></i>Usuarios
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/index">Agregar usuario</a></li>
                            <li><a href="<?= base_url() ?>welcome/users_view">Ver usuario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-th-large"></i>Invetario
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/inven">Ver inventario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-plus-sign"></i>Productos
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prod">Agregar producto</a></li>
                            <li><a href="<?= base_url() ?>welcome/prod_view">Ver producto</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-briefcase"></i>Proveedor
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prov">Agregar proveedor</a></li>
                            <li><a href="<?= base_url() ?>welcome/provs_view">Ver proveedor</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
      </div>
		  <div class="col-md-9">

		  	<div class="row">
          <div class="row">
            <div class="col-md-12 panel-info">
              

              <div class="content-box-large box-with-header" style="border-radius: 20px;">
              <div class="panel-title " style="margin-bottom:50px"><h1>Agregar Proveedor</h1></div>

              <div>

                <div class="row">
                  <div class="col-sm-4">
                  <label>Nombre de Proveedor/Empresa</label>
                    <input type="text" id="nom_emp" name="nom_emp" class="form-control">
                  </div>
                  <div class="col-sm-4">
                  <label>Frecuencia entrega de producto</label>
                    <select id="freq_ent" name="freq_ent" class="form-control">
                    <option value="">--Elegir Opcion--</option>
                      <option value="Semanal">Semanal</option>
                      <option value="Mensual">Mensual</option>
                      <option value="Anual">Anual</option>
                    </select>
                  </div>
                  <div class="col-sm-4">
                  <label>Telefono Contacto</label>
                    <input type="text" id="tel" name="tel" class="form-control">
                  </div>

                </div>

                <hr>
                <div class="row">
                   
                  
                  <div class="col-sm-4">
                  <label>Direccion</label>
                    <textarea id="dir" name="dir" rows="4" cols="50" class="form-control">
                   </textarea>
                  </div>
                  <div class="col-sm-4">
                   <label>Descripcion (opcional)</label>
                    <textarea id="desc" name="desc" rows="4" cols="50" class="form-control">
                   </textarea>
                  </div>
                </div>
                </hr>
                <div class="row">
                  <div class="col-sm-12" style="margin-top: 25px;">
                  <button type="submit" style="width: 100%;" class="btn btn-primary" id="guardar" name="guardar" onclick="subir()">Aceptar</button>
                  </div>
                </div>
              </div>


            </div>
            </div>
          </div>
		  	</div>

		  </div>
		</div>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <footer>
         <div class="container">

            <div class="copy text-center">
               Copyright 2021 <a href='#'>Website</a>
            </div>

         </div>
      </footer>

<script type="text/javascript">
  
  function subir(data){
    $.ajax({
      "url"     : "http://localhost/metodologia/usuarios/insert_prov",
      "type"    : "post",
      "data"    : {
        "nom_emp"       : $( '#nom_emp' ).val(),
        "freq_ent"       : $( '#freq_ent' ).val(),
        "tel"       : $( '#tel' ).val(),
        "dir"    : $( '#dir' ).val(),
        "desc"      : $( '#desc' ).val()
      },
      "dataType" : "json",
      "success"  : function( json ){


          alert("Proveedor Registrado con exito");

          setTimeout(
                  function() 
                  {
                     location.reload();
                  }, 0001);


      } 

    });
}



</script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url() ?>static/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>static/js/custom.js"></script>
  </body>
</html>
