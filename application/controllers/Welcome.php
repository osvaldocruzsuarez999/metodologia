<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	  public function __construct() {
		parent::__construct();
        $this->load->library(array('session'));
		$this->load->helper("url_helper");
    }

	public function index()
	{

	if($this->session->userdata('loggeado')){
			$this->load->view('users');
		}else{
            redirect(base_url());
		}
	}
 
	public function users_view()
	{
		if($this->session->userdata('loggeado')){
			$this->load->view('usuarios_view');
		}else{
            redirect(base_url());
		}
	}


	public function prod()
	{
		if($this->session->userdata('loggeado')){
			$this->load->view('prod');
		}else{
            redirect(base_url());
		}
	}

	public function prod_view()
	{
		if($this->session->userdata('loggeado')){
			$this->load->view('prod_view');
		}else{
            redirect(base_url());
		}
	}

	public function prov()
	{
		if($this->session->userdata('loggeado')){
			$this->load->view('provs');
		}else{
            redirect(base_url());
		}
	}
	public function provs_view()
	{
		if($this->session->userdata('loggeado')){
			$this->load->view('provs_view');
		}else{
            redirect(base_url());
		}
	}

		public function inven()
	{
		if($this->session->userdata('loggeado')){

			$this->load->view('inven');	
		}else{
			redirect(base_url());
		}
		
	}
}
