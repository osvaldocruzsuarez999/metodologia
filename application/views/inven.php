<!DOCTYPE html>
<html>
  <head>
    <title>Sistema Inventarios</title>
 <script>
    var base_url = "<?= base_url() ?>";
  </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?= base_url() ?>static/css/styles.css" rel="stylesheet">

    <style type="text/css">
        
      @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

body {
  background-color: #2c3742;
  font-family: "Roboto", helvetica, arial, sans-serif;
  font-size: 16px;
  font-weight: 400;
  text-rendering: optimizeLegibility;
}

div.table-title {
   display: block;
  margin: auto;
  max-width: 600px;
  padding:5px;
  width: 100%;
}

.table-title h3 {
   color: #fafafa;
   font-size: 30px;
   font-weight: 400;
   font-style:normal;
   font-family: "Roboto", helvetica, arial, sans-serif;
   text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
   text-transform:uppercase;
}


/*** Table Styles **/

.table-fill {
  background: white;
  border-radius:3px;
  border-collapse: collapse;
  height: auto;
  margin: auto;
  max-width: 800px;
  padding:5px;
  width: 100%;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
  animation: float 5s infinite;
}
 
th {
  color:#D5DDE5;
  background:#1b1e24;
  border-bottom:4px solid #9ea7af;
  border-right: 1px solid #343a45;
  font-size:15px;
  font-weight: 50;
  padding:15px;
  text-align:left;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  vertical-align:middle;
}

th:first-child {
  border-top-left-radius:3px;
}
 
th:last-child {
  border-top-right-radius:3px;
  border-right:none;
}
  
tr {
  border-top: 1px solid #C1C3D1;
  border-bottom-: 1px solid #C1C3D1;
  color:#666B85;
  font-size:16px;
  font-weight:normal;
  text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
}
 
tr:hover td {
  background:#4E5066;
  color:#FFFFFF;
  border-top: 1px solid #22262e;
}
 
tr:first-child {
  border-top:none;
}

tr:last-child {
  border-bottom:none;
}
 
tr:nth-child(odd) td {
  background:#EBEBEB;
}
 
tr:nth-child(odd):hover td {
  background:#4E5066;
}

tr:last-child td:first-child {
  border-bottom-left-radius:3px;
}
 
tr:last-child td:last-child {
  border-bottom-right-radius:3px;
}
 
td {
  background:#FFFFFF;
  padding:15px;
  text-align:left;
  vertical-align:middle;
  font-weight:300;
  font-size:15px;
  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
  border-right: 1px solid #C1C3D1;
}

td:last-child {
  border-right: 0px;
}

th.text-left {
  text-align: left;
}

th.text-center {
  text-align: center;
}

th.text-right {
  text-align: right;
}

td.text-left {
  text-align: left;
}

td.text-center {
  text-align: center;
}

td.text-right {
  text-align: right;
}

.resta_campo {
  width: 100px;
}


    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                   <h1><a href="index.html">LOGO</a></h1>
                </div>
             </div>
             <div class="col-md-5">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="input-group form">
                         <input type="text" class="form-control" placeholder="Buscar...">
                         <span class="input-group-btn">
                           <button class="btn btn-primary" type="button">Buscar</button>
                         </span>
                    </div>
                  </div>
                </div>
             </div>
             <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                          <ul class="dropdown-menu animated fadeInUp">
                            <li><a href="login.html">Salir</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                </div>
             </div>
          </div>
       </div>
  </div>

    <div class="page-content" style="background-color: #f7f7f7;">
      <div class="row">
      <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-user"></i>Usuarios
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/index">Agregar usuario</a></li>
                            <li><a href="<?= base_url() ?>welcome/users_view">Ver usuario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-th-large"></i>Invetario
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/inven">Ver inventario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-plus-sign"></i>Productos
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prod">Agregar producto</a></li>
                            <li><a href="<?= base_url() ?>welcome/prod_view">Ver producto</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-briefcase"></i>Proveedor
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prov">Agregar proveedor</a></li>
                            <li><a href="<?= base_url() ?>welcome/provs_view">Ver proveedor</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
      </div>
		  <div class="col-md-10">

            <div class="col-md-11 panel-info">
              <div class="panel-title "><h1>Inventariado</h1></div>

<div class="modal" id="desc" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Descripción</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="data-title"></p>
      </div>
    </div>
  </div>
</div>


    <div class="table-title" style="margin-top: 25px;">

</div>
<div style="width:auto; height: 300px; overflow: auto; padding: 50px; margin-bottom: 100px;">

<table class="table-fill">
<thead>
<tr style="position: sticky;">
<th class="text-left">Nombre del Producto</th>
<th class="text-left">Marca</th>
<th class="text-left">Precio de venta</th>
<th class="text-left">Nombre Proveedor</th>
<th class="text-left">Descripción</th>
<th class="text-left">Cantidad Total</th>
<th class="text-left">Producto Vendido</th>
<th class="text-left">Acciones</th>
</tr>
</thead>
<tbody class="table-hover">
<?php  

echo "   <tr> "; 

$link = mysqli_connect("localhost", "root", "", "meto");

$result=mysqli_query($link,"SELECT * FROM inventario ");

foreach ($result as $results =>$item) {
echo '<tr>';
echo '<td class="text-left">'.$item['nom_prod'].'</td>';
echo '<td class="text-left">'.$item['marca'].'</td>';
echo '<td class="text-left"> $ '.$item['precio'].'</td>';
echo '<td class="text-left">'.$item['nom_emp'].'</td>';
echo '<td class="text-left"><button data-toggle="modal" data-target="#desc" onclick="desc(`'.$item['desc'].'`)">Ver</button></td>';
echo '<td class="text-left">'.$item['cantidad'].'</td>';
echo '<td class="text-left"><input class="resta_campo" type="number" name="resta" id="resta" min="1" max='.$item['cantidad'].'></td>';
echo '<td><button onclick="resta(`'.$item['nom_prod'].'`,`'.$item['cantidad'].'`)">Restar</button></td>';
echo '</tr>';
}
?>
</tbody>
</table>


</div>

             
            </div>
          </div>
		</div>
    </div>
    <br><br><br>
    <footer>
         <div class="container">

            <div class="copy text-center">
               Copyright 2021 <a href='#'>Website</a>
            </div>

         </div>
      </footer>

<script type="text/javascript">
  
  function subir(data){
    $.ajax({
      "url"     : "http://localhost/metodologia/usuarios/insert",
      "type"    : "post",
      "data"    : {
        "app"       : $( '#app' ).val(),
        "apm"       : $( '#apm' ).val(),
        "nom"       : $( '#nom' ).val(),
        "correo"    : $( '#correo' ).val(),
        "pass"      : $( '#pass' ).val()
      },
      "dataType" : "json",
      "success"  : function( json ){


          alert("Usuario Registrado con exito");

          setTimeout(
                  function() 
                  {
                     location.reload();
                  }, 0001);


      } 

    });
}

    function desc(llave){
      $("#data-title").html(llave);
      
    }


    function resta(nom_prod, cant){

    var resta = parseFloat($('#resta').val());

if (resta > cant || resta <= 0) {

  alert("No se puede restar esa cantidad");

}else{

var total = parseFloat(cant - resta);


      $.ajax({
        "url"     : "http://localhost/metodologia/usuarios/resta",
        "type"  : "post",
      "data"  : {
        "nom_prod" : nom_prod,
        "new_cant" : total,

      },  
      "dataType" : "json",
      "success"  : function( json ){


          alert("Actualizacion exitosa");
           setTimeout(
                  function() 
                  {
                     location.reload();
                  }, 0001);


      } 




      });

}


     
      

    }



</script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url() ?>static/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>static/js/custom.js"></script>
  </body>
</html>
