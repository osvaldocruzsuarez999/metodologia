<?php

class Login_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function auth($data)
    {
        $query = $this->db->get_where('usuarios', array('correo' => $data['usuario'], 'contraseña' => $data['contraseña']));
     
        if($query->num_rows() > 0){
                $row = $query->row();
                if( $row->id){
                    $obj['mensaje']   = "BIENVENIDO " .$row->nom. " ";
                    $obj['user']      = $data['usuario'];
                    $obj['valor']     = true;
                    $obj['url']       = "http://localhost/metodologia/welcome";

                }else{
                    
                    $obj['mensaje']   = " USUARIO INACTIVO ";
                    $obj['url']       = "http://localhost/metodologia/";
                    $obj['user']      = null;
                    $obj['valor']     = false;

                }


        }else{

            $obj['mensaje']   = " USUARIO Y/O CONTRASEÑA INCORRECTA ";
            $obj['url']       = "http://localhost/metodologia/";
            $obj['user']      = $data['usuario'];
            $obj['valor']     = false;
            
        }

        return $obj;


    }
    
}

?>