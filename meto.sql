  create database meto; 
  use meto ;
  -- phpMyAdmin SQL Dump
  -- version 4.9.2
  -- https://www.phpmyadmin.net/
  --
  -- Servidor: 127.0.0.1:3306
  -- Tiempo de generación: 31-03-2021 a las 20:47:18
  -- Versión del servidor: 10.4.10-MariaDB
  -- Versión de PHP: 7.3.12

  SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
  SET AUTOCOMMIT = 0;
  START TRANSACTION;
  SET time_zone = "+00:00";


  /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
  /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
  /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
  /*!40101 SET NAMES utf8mb4 */;

  --
  -- Base de datos: `meto`
  --

  -- --------------------------------------------------------

  --
  -- Estructura Stand-in para la vista `inventario`
  -- (Véase abajo para la vista actual)
  --
  DROP VIEW IF EXISTS `inventario`;
  CREATE TABLE IF NOT EXISTS `inventario` (
  `id` int(10)
  ,`nom_prod` varchar(50)
  ,`marca` varchar(50)
  ,`precio` int(250)
  ,`nom_emp` varchar(50)
  ,`desc` longtext
  ,`cantidad` int(10)
  );

  -- --------------------------------------------------------

  --
  -- Estructura de tabla para la tabla `productos`
  --

  DROP TABLE IF EXISTS `productos`;
  CREATE TABLE IF NOT EXISTS `productos` (
    `id` int(10) NOT NULL,
    `cantidad` int(10) DEFAULT NULL,
    `nom_prod` varchar(50) DEFAULT NULL,
    `marca` varchar(50) DEFAULT NULL,
    `costo` int(250) DEFAULT NULL,
    `precio` int(250) DEFAULT NULL,
    `prov` varchar(50) DEFAULT NULL,
    `num_ser` varchar(50) DEFAULT NULL,
    `desc` longtext DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

  --
  -- Volcado de datos para la tabla `productos`
  --

  INSERT INTO `productos` (`id`, `cantidad`, `nom_prod`, `marca`, `costo`, `precio`, `prov`, `num_ser`, `desc`) VALUES
  (1, 200, 'Doritos', 'Sabritas', 200, 400, 'Sabritas', '2531513', 'Papitas con forma de triangulo'),
  (2, 150, 'Libro de matematicas OwO', 'Vaporware', 15000, 500, 'Neolearning', '01880000', 'Libro de Calculo Integral');

  -- --------------------------------------------------------

  --
  -- Estructura de tabla para la tabla `proveedores`
  --

  DROP TABLE IF EXISTS `proveedores`;
  CREATE TABLE IF NOT EXISTS `proveedores` (
    `id` int(10) NOT NULL,
    `nom_emp` varchar(50) DEFAULT NULL,
    `freq_ent` varchar(50) DEFAULT NULL,
    `tel` varchar(50) DEFAULT NULL,
    `dir` longtext DEFAULT NULL,
    `desc` longtext DEFAULT NULL,
    `status` int(5) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

  --
  -- Volcado de datos para la tabla `proveedores`
  --

  INSERT INTO `proveedores` (`id`, `nom_emp`, `freq_ent`, `tel`, `dir`, `desc`, `status`) VALUES
  (1, 'Neolearning', 'Anual', '4421857167', 'Santiago de los llanos grandes\nVillas de santiago', 'UwU', 1);

  -- --------------------------------------------------------

  --
  -- Estructura de tabla para la tabla `usuarios`
  --

  DROP TABLE IF EXISTS `usuarios`;
  CREATE TABLE IF NOT EXISTS `usuarios` (
    `id` int(10) NOT NULL,
    `app` varchar(50) DEFAULT NULL,
    `apm` varchar(50) DEFAULT NULL,
    `nom` varchar(50) DEFAULT NULL,
    `correo` varchar(50) DEFAULT NULL,
    `contraseña` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

  --
  -- Volcado de datos para la tabla `usuarios`
  --

  INSERT INTO `usuarios` (`id`, `app`, `apm`, `nom`, `correo`, `contraseña`) VALUES
  (1, 'TEST', 'TEST', 'TEST', 'test@gmail.com', '123456'),
  (2, 'Cruz', 'Suarez', 'Osvaldo', 'el.canicas_69@gmail.com', '123456');

  -- --------------------------------------------------------

  --
  -- Estructura para la vista `inventario`
  --
  DROP TABLE IF EXISTS `inventario`;

  CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventario`  AS  select `p`.`id` AS `id`,`p`.`nom_prod` AS `nom_prod`,`p`.`marca` AS `marca`,`p`.`precio` AS `precio`,`r`.`nom_emp` AS `nom_emp`,`p`.`desc` AS `desc`,`p`.`cantidad` AS `cantidad` from (`productos` `p` join `proveedores` `r`) where `p`.`prov` = `r`.`nom_emp` ;
  COMMIT;

  /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
  /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
  /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
