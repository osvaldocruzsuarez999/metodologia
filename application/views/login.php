<!DOCTYPE html>
<html>
  <head>
    <title>Sistema Inventarios</title>
 <script>
    var base_url = "<?= base_url() ?>";
  </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?= base_url() ?>static/css/styles.css" rel="stylesheet">
    <link href="<?= base_url() ?>static/css/images.css" rel="stylesheet">
    
    


  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html"><img src="<?= base_url()?>static/images/256x256.png" width="3%" height="3%"/> Inventario</a><h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

  		      
			<div class="col-md-8 col-md-offset-2">
				<div class="login-wrapper">
			      <div class="box" margin='100'>
              <img src="<?= base_url()?>static/images/256x256.png" class="imgRedonda fadeIn first" />
                <form style="margin-left:100px;margin-right:100px">
                  <div class="action"> 
                    <input class="form-control fadeIn second" type="email" placeholder="Correo" id="usu">
                  </div>
                  <div class="action">
			                <input class="form-control fadeIn third" type="password" placeholder="Contraseña"  id="pass">
			            </div>
                  <div class="action">
			                    <button  style="width: 100%;" type="button" class="btn btn-primary signup fadeIn fourth" onclick="enviar_datos()">Entrar</button>
			            </div>
			          </form>      
              
			      </div>
			  </div>
		  </div>
  </body>
</html>
<script type="text/javascript">
	


   function enviar_datos(){

alert($( '#usu' ).val());
alert($( '#pass' ).val());




    $.ajax({
      "url"     : "http://localhost/metodologia/login/login",
      "type"    : "post",
      "data"    : {
        "usuario"       : $( '#usu' ).val(),
        "contraseña"    : $( '#pass' ).val()
      },

      "dataType" : "json",
      "success"  : function( json ){
                if(json.valor == true){

                    window.location.replace(json.url);

                }else{
                    
                    alert(json.mensaje); 
                }

      } 

    });



}


</script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url() ?>static/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>static/js/custom.js"></script>
  
