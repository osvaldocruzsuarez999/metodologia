<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        $this->load->view('login');
    }

    public function __construct(){
        parent:: __construct();
        $this->load->model('Login_model');
        $this->load->library(array('session'));
    
    }

    public function login(){

        $usuario    = ( $this->input->post( "usuario" )); 
        $contrasena =  strtoupper (($this->input->post( "contraseña" )));

    
        if($usuario == null || $contrasena == null){

            $obj[ "mensaje" ] = "FAVOR DE LLENAR TODOS LOS CAMPOS";
            $obj[ "valor" ]   = false;
            $obj['url']       = "http://localhost/metodologia/";

        }else{

            $data = array(
                'usuario'    => $usuario,
                'contraseña' => $contrasena
            );

            $obj = $this->Login_model->auth($data);

            if($obj['user'] != null){
            
                $credenciales = array(

                    "usuario"   =>  $usuario,
                    "loggeado"  =>  true
                );

                $this->session->set_userdata($credenciales);
                    

            }

        }

        
        $this->output->set_content_type( "application/json" );
        echo json_encode( $obj );
     
    }

    public function logout(){
        $data = array('usuario', 'loggeo');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();

        $obj['url']       = "http://localhost/metodologia/";
        $this->output->set_content_type( "application/json" );
        echo json_encode( $obj );

    }


}
