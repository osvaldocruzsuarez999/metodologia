<!DOCTYPE html>
<html>
  <head>
    <title>Sistema de Inventario</title>
    <script>
    var base_url = "<?= base_url() ?>";
  </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?= base_url() ?>static/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color: #EAEDED;">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
                <h1><a href="index.html"><img src="<?= base_url()?>static/images/256x256.png" width="5%" height="5%"/> Inventario</a><h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="login.html">Salir</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
      <div class="row">
      <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-user"></i>Usuarios
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/index">Agregar usuario</a></li>
                            <li><a href="<?= base_url() ?>welcome/users_view">Ver usuario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-th-large"></i>Invetario
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/inven">Ver inventario</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-plus-sign"></i>Productos
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prod">Agregar producto</a></li>
                            <li><a href="<?= base_url() ?>welcome/prod_view">Ver producto</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-briefcase"></i>Proveedor
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?= base_url() ?>welcome/prov">Agregar proveedor</a></li>
                            <li><a href="<?= base_url() ?>welcome/provs_view">Ver proveedor</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
      </div>
		  <div class="col-md-9">

		  	<div class="row">
          <div class="row">
            <div class="col-md-12 panel-info">
              
              <div class="content-box-large box-with-header" style="border-radius: 20px;">
              <div class="panel-title "style="margin-bottom:50px" ><h1>Agregar Producto</h1></div>

                <div>

                <div class="row">
                  <div class="col-sm-4">
                  <label>Cantidad</label>
                    <input type="number" class="form-control" id="cantidad" name="cantidad">
                  </div>
                   <div class="col-sm-4">
                   <label>Nombre</label>
                    <input type="text" class="form-control" id="nom_prod" name="nom_prod">
                  </div>
                   <div class="col-sm-4">
                   <label>Marca</label>
                    <input type="text" class="form-control" id="marca" name="marca">
                  </div>
                </div>

                <hr>
                <div class="row">
                 <div class="col-sm-4">
                   <label>Costo de Adquisicion</label>
                    <input type="number" class="form-control" id="costo" name="costo">
                  </div>
                  <div class="col-sm-4">
                   <label>Precio</label>
                    <input type="number" class="form-control" id="precio" name="precio">
                  </div>
                  <div class="col-sm-4">
                   <label>Proveedor</label>
                    <select name="prov" id="prov" class="form-control">
                      <option value="">--Elegir Opcion--</option>
                      <?php  

    $link = mysqli_connect("localhost", "root", "", "meto");

      $result=mysqli_query($link,"SELECT * FROM proveedores");

  foreach ($result as $results =>$item) {

echo '<option value="'.$item['nom_emp'].'">'.$item['nom_emp'].'</option>';
}

            ?>

                    </select>
                  </div>

                 
                  
                  </div>
                  
                <hr>
                <div class="row">
                 <div class="col-sm-4">
                   <label>N° de serie</label>
                    <input type="text" class="form-control" id="num_ser" name="num_ser">
                  </div>
                  <div class="col-sm-4">
                   <label>Descripcion</label>
                   <textarea id="desc" name="desc" rows="4" cols="50" class="form-control">
                   </textarea>
                </div>
                <hr>
                  <div class="col-sm-12" style="margin-top: 25px;">
                  <button style="width: 100%;" type="submit" class="btn btn-primary" onclick="subir()">Aceptar</button>
                </div>
              </div>
            </div>
            </div>
          </div>
		  	</div>

		  </div>
		</div>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <footer>
         <div class="container">

            <div class="copy text-center">
               Copyright 2021 <a href='#'>Website</a>
            </div>

         </div>
      </footer>
      <script type="text/javascript">
  
  function subir(data){
    $.ajax({
      "url"     : "http://localhost/metodologia/usuarios/insert_prod",
      "type"    : "post",
      "data"    : {
        "cantidad"       : $( '#cantidad' ).val(),
        "nom_prod"       : $( '#nom_prod' ).val(),
        "marca"       : $( '#marca' ).val(),
        "costo"       : $( '#costo' ).val(),
        "precio"    : $( '#precio' ).val(),
        "prov"      : $( '#prov' ).val(),
        "num_ser"    : $( '#num_ser' ).val(),
        "desc"    : $( '#desc' ).val()
      },
      "dataType" : "json",
      "success"  : function( json ){


          alert("Producto Registrado con exito");

          setTimeout(
                  function() 
                  {
                     location.reload();
                  }, 0001);


      } 

    });
}



</script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="<?= base_url() ?>static/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>static/js/custom.js"></script>
  </body>
</html>
